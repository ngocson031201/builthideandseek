
Banner: 		Quảng cáo nhỏ thường đặt ở phía tên hoặc dưới màn hình
Interstitial: 	Quảng cáo xen kẽ gọi khi chuyển cảnh, chuyển màn hình (Có thể bỏ qua)
Rewarded: 		Quảng cáo yêu cầu người chơi xem hết quảng cáo để có thể nhận thưởng (Bỏ qua mất thưởng)

Prefabs
- Ads: - Quản lý quảng cáo, Send Event,...
	 - Đặt ở main scene
- BtnVersion: - Nút hiển thị số phiên bản
		  - Tap 10 lần để show debugger
		  - Đặt ở trong Canvas chính

Function
- RemoveBannerAds: Xóa banner
- RemoveInterstitialAds: Xóa quảng cáo xen kẽ
- RemoveRewardAds: Xóa quảng cáo nhận thưởng 

- CanShowBannerAds: Check banner
- CanShowInterstitialAds: Check quảng cáo xen kẽ
- CanShowRewardedAds: Check quảng cáo nhận thưởng 

- ShowBanner, HideBanner: Ẩn hiện banner
- ShowInterstitial: Hiện quảng cáo xen kẽ
- ShowRewarded: Hiện quảng cáo tặng thưởng


