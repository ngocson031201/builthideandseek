/* @ThanhD143 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Version : MonoBehaviour
{
    public Text txtVersion;
    private int clickCounter = 0;
    private float timeCounter;

    private void Start()
    {
        clickCounter = 0;
        timeCounter = 0;
        txtVersion.text = "Version: " + Application.version;
    }

    public void ShowDebug()
    {
        clickCounter++;
        if (clickCounter > 10)
        {
            ManagerAds.ins.debugger.SetActive(true);
            clickCounter = 0;
        }
    }

    private void FixedUpdate()
    {
        if (clickCounter > 0)
        {
            timeCounter += Time.deltaTime;
            if (timeCounter > 10f)
            {
                timeCounter = 0;
                clickCounter = 0;
            }
        }
    }
}