using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    public RectTransform DefaultUI, NoAdsUI, shop, shopIAP, daily, Setting;
    private void Start()
    {
        DefaultUI.DOAnchorPos(Vector2.zero, 0.5f);
        if (UIController.instance.day == 0)
        {
            StartCoroutine(CounDownTimeDaily());
        }

    }


    IEnumerator CounDownTimeDaily()
    {
        yield return new WaitForSeconds(.5f);
        OpenDaily();
    }

    public void OpenSetting()
    {
        Setting.DOAnchorPos(new Vector2(0, 0), 0.25f);
    }


    public void CloseSetting()
    {
        Setting.DOAnchorPos(new Vector2(-1900, 0), 0.25f).SetEase(Ease.InOutCubic);
    }

    public void OpenDaily()
    {
        daily.DOAnchorPos(new Vector2(0, 0), 0.25f);

    }

    public void CloseDaily()
    {
        daily.DOAnchorPos(new Vector2(0, 1350), 0.25f).SetEase(Ease.InOutCubic);
    }

    public void AdsButton()
    {
        // DefaultUI.DOAnchorPos(new Vector2(800, -800), 0.25f);
        NoAdsUI.DOAnchorPos(new Vector2(0, 0), 0.25f);
    }

    public void CloseAdsButton()
    {
        NoAdsUI.DOAnchorPos(new Vector2(800, 0), 0.25f);
        DefaultUI.DOAnchorPos(new Vector2(0, 0), 0.25f);

    }

    public void ShopButton()
    {

        shop.DOAnchorPos(new Vector2(0, 0), 0.25f);
    }

    public void CloseShopButton()
    {
        shop.DOAnchorPos(new Vector2(0, -1350), 0.25f).SetEase(Ease.InOutCubic);
    }

    public void ShopIAPButton()
    {

        shopIAP.DOAnchorPos(new Vector2(0, 0), 0.25f);
    }

    public void CloseShopIAPButton()
    {
        shopIAP.DOAnchorPos(new Vector2(-800, 0), 0.25f).SetEase(Ease.InOutCubic);
    }

}
