﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using System.IO;

public class GameManager : MonoBehaviour
{
    
    public static GameManager instance;
    [SerializeField] string SpawnPointMapTag = "SpawnPointMap";
    public MapData[] mapDatas;
    public int level=0;
    GameObject map;
    public Text NumburTextLevel;
    public static int checkIsUserPlayGame;
    private void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait; //khóa dọc màn hình
        checkIsUserPlayGame = 0;
        CreateMapFirst();
        instance = this;
        //if(Prefs.BANNERADS==0)
        //{
        //    ManagerAds.ins.ShowBanner();
        //}

    }


    public void SaveSkin(int idskin)
    {
        Save ss = new Save(idskin);
        SaveSystem.SaveData(ss, StaticVar.Path_SAVESHOP);
       // Debug.Log("itemindex" + idskin);
    }

    public List<int> LoadSkinOwned()
    {

        if (File.Exists(StaticVar.Path_SAVESHOP))
        {

            Save data = SaveSystem.LoadData<Save>(StaticVar.Path_SAVESHOP);
            return data.skinOwned;
        }
        else
        {
            List<int> skinowned = new List<int>();
            skinowned.Clear();
            return skinowned;
        }

    }

    public int CustomRandomPersenForChest(int[] allRate)
    {
        int total = 0;
        foreach (var i in allRate)
        {
            total += i;
        }

        int r = UnityEngine.Random.Range(0, total);
        for (int i = 0; i < allRate.Length; i++)
        {
            if (r <= allRate[i])
            {
                return i;
            }
            else
            {
                r -= allRate[i];
            }
        }
        return -1;
    }

    public void CreateMapFirst()
    {
        level = Prefs.LEVEL;

        int indexLevel = level + 1;
        NumburTextLevel.text = indexLevel.ToString();// hiển thị text xem là level mấy !!!

        GameObject MapPoint = GameObject.FindGameObjectWithTag(SpawnPointMapTag);
        map = Instantiate(mapDatas[Prefs.LEVEL].mapPrefab);
        map.transform.position = MapPoint.transform.position;
        map.transform.rotation = MapPoint.transform.rotation;
        PlayerPrefs.Save();
    }

    public void LevelInCrease()
    {
        Destroy(map);
        SceneManager.LoadScene(0);
        level++;
        Prefs.LEVEL = level;
        int indexLevel = level + 1;
        NumburTextLevel.text = indexLevel.ToString();// hiển thị text xem là level mấy !!!

        GameObject MapPoint = GameObject.FindGameObjectWithTag(SpawnPointMapTag);
        map = Instantiate(mapDatas[Prefs.LEVEL].mapPrefab);
        map.transform.position = MapPoint.transform.position;
        map.transform.rotation = MapPoint.transform.rotation;
        PlayerPrefs.Save();
    }

    public void WatchVideoPassLevel()
    {
        ManagerAds.ins.ShowRewarded((x) =>
        {
            if (x)
            {
                LevelInCrease();
            }
        });
    }    

    public void AgainLevel()
    {
        Destroy(map);
        SceneManager.LoadScene(0);
        Prefs.LEVEL = level;

        int indexLevel = level + 1;
        NumburTextLevel.text = indexLevel.ToString();// hiển thị text xem là level mấy !!!

        GameObject MapPoint = GameObject.FindGameObjectWithTag(SpawnPointMapTag);
        map = Instantiate(mapDatas[Prefs.LEVEL].mapPrefab);
        map.transform.position = MapPoint.transform.position;
        map.transform.rotation = MapPoint.transform.rotation;
        PlayerPrefs.Save();
    }
}
