using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class DOColorScript : MonoBehaviour
{
    private Material DefaultMaterials;
    public Color color;
    void Start()
    {
        DefaultMaterials = GetComponent<MeshRenderer>().material;
        DefaultMaterials.DOColor(color, .6f).SetEase(Ease.InOutFlash).SetLoops(-1, LoopType.Yoyo);
    }

}
