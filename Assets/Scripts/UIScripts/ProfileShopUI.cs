﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;

public class ProfileShopUI : MonoBehaviour
{

    #region Singleton: Profile
    public static ProfileShopUI instance;
    GameObject g;
    int newSelectedIndex, previuosSelectedIndex;
    [SerializeField] Color ActiveAvatarColor;
    [SerializeField] Color DefaultAvatarColor;

    [SerializeField] Image CurrentAvatar;
    public static int IDIndexSkin;
    bool checkDailySkin01;
    bool checkDailySkin02;
    private void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public class Avatar
    {
        public int Id;
        public Sprite Image;
    }

    public List<Avatar> AvartarsList;
    public List<Image> ImageDaily;
    [SerializeField] GameObject AvatarUITemplate;
    [SerializeField] Transform AvatarsScrollView;

    
    private void Start()
    {


        GetAvailableAvatars();
        Load();

        if (Prefs.CHECKUNLOCKALLSKIN > 0)
        {
            UnLockAllSkin();
            //hiển thị dấu tích
            Shop.instance.ShopScrollView.GetChild(40).GetChild(4).gameObject.SetActive(true);
            //hiển thị background tối đằng sau nhân vật
            Shop.instance.ShopScrollView.GetChild(40).GetChild(0).gameObject.SetActive(true);
        }

        if (Prefs.CHECKSTARTERPACK > 0)
        {
            StarterPack();
        }

        if (Prefs.SKINLOADINPROGRESS > 0)
        {
            skinLoadInProgress();
        }



        newSelectedIndex = previuosSelectedIndex = Prefs.AVATARDEFAULT;
        addImageDailyskin01();
        addImageDailyskin02();



        Debug.Log(" Prefs.AVATARDEFAULT" + Prefs.AVATARDEFAULT);
    }

    void GetAvailableAvatars()
    {
        for (int i = 0; i < Shop.instance.ShopItemList.Count; i++)
        {
            //nếu nó được mua thì lấy ảnh nó gán vào ảnh bên này
            if (Shop.instance.ShopItemList[i].IsPurchased)
            {
                AddAvatar(Shop.instance.ShopItemList[i].Image, Shop.instance.ShopItemList[i].id);


            }
            //lưu trữ dữ liệu sẽ ở đây
        }
        SelectAvatar(newSelectedIndex);
    }

    public void Load()
    {
        // AvartarsList.Clear();
        //làm sao cho nó không spawn lại các image khi mình bấm vào lại shop , kiểu spawn double lên 
        if (GameManager.checkIsUserPlayGame > 0)
        {
            return;
        }

        for (int i = 1; i < GameManager.instance.LoadSkinOwned().Count; i++)
        {

            AddAvatar(Shop.instance.ShopItemList[GameManager.instance.LoadSkinOwned()[i]].Image, GameManager.instance.LoadSkinOwned()[i] + 1);

            //hiển thị là đã mua bằng dấu tích
            Shop.instance.ShopScrollView.GetChild(GameManager.instance.LoadSkinOwned()[i] + 1).GetChild(4).gameObject.SetActive(true);

            //hiển thị background tối đằng sau nhân vật

            Shop.instance.ShopScrollView.GetChild(GameManager.instance.LoadSkinOwned()[i] + 1).GetChild(0).gameObject.SetActive(true);
        }
    }

    public void UnLockAllSkin()
    {

        for (int i = 1; i < Shop.instance.ShopItemList.Count; i++)
        {
            if (Shop.instance.ShopItemList[i].IsPurchased == true)
            {
                continue;
            }
            AddAvatar(Shop.instance.ShopItemList[i].Image, Shop.instance.ShopItemList[i].id);
            //hiển thị dấu tích
            Shop.instance.ShopScrollView.GetChild(i).GetChild(4).gameObject.SetActive(true);
            //hiển thị background tối đằng sau nhân vật
            Shop.instance.ShopScrollView.GetChild(i).GetChild(0).gameObject.SetActive(true);
        }
    }

    public void StarterPack()
    {
        for (int i = 2; i < 6; i++)
        {
            AddAvatar(ImageDaily[i].sprite, 41+i);//thay đổi giá trị ID ở đây là nó sẽ đc xuất hiện bên trong shop
        }
        SelectAvatar(newSelectedIndex);
    }

    public void addImageDailyskin01()
    {
        if (checkDailySkin01 == true)
            return;

        if (UIController.instance.Daily[1].activeInHierarchy == true)
        {
            AddAvatar(ImageDaily[0].sprite, 41);//thay đổi giá trị ID ở đây là nó sẽ đc xuất hiện bên trong shop
            checkDailySkin01 = true;
        }
        SelectAvatar(newSelectedIndex);

    }

    public void addImageDailyskin02()
    {
        if (checkDailySkin02 == true)
            return;

        if (UIController.instance.Daily[4].activeInHierarchy == true)
        {
            AddAvatar(ImageDaily[1].sprite, 42);//thay đổi giá trị ID ở đây là nó sẽ đc xuất hiện bên trong shop
            checkDailySkin02 = true;
        }
        SelectAvatar(newSelectedIndex);
    }

    public void skinLoadInProgress()
    {
        AddAvatar(ImageDaily[6].sprite, 6);//thay đổi giá trị ID ở đây là nó sẽ đc xuất hiện bên trong shop
        SelectAvatar(newSelectedIndex);
    }



    public void addImageChest()
    {
        AddAvatar(ImageDaily[1].sprite, 42);//Lấy tạm giá trị image của Daily ở hàm bên trên
        SelectAvatar(newSelectedIndex);
    }


    public void AddAvatar(Sprite img, int id)
    {
        if (AvartarsList == null)
        {
            AvartarsList = new List<Avatar>();
        }
        Avatar av = new Avatar() { Image = img, Id = id };

        //theem av vào list

        AvartarsList.Add(av);
        //add avatar in the ui scroll view;

        g = Instantiate(AvatarUITemplate, AvatarsScrollView);
        g.transform.GetChild(0).GetComponent<Image>().sprite = av.Image;
        //add click Event
        g.transform.GetComponent<Button>().AddEventListener(AvartarsList.Count - 1, OnAvatarClick);

        g.transform.GetComponent<Button>().AddEventListener(AvartarsList.Count - 1, checkIsUserPlayGamee);

    }



    void checkIsUserPlayGamee(int index)
    {
        index++;
        GameManager.checkIsUserPlayGame = index;
    }

    void OnAvatarClick(int AvatarIndex)
    {
        SelectAvatar(AvatarIndex);
        Prefs.AVATARDEFAULT = AvatarIndex;
        Debug.Log(" Prefs.AVATARDEFAULT000" + Prefs.AVATARDEFAULT);
    }

    void SelectAvatar(int avatarIndex)
    {
        previuosSelectedIndex = newSelectedIndex;
        newSelectedIndex = avatarIndex;
        
        AvatarsScrollView.GetChild(previuosSelectedIndex).GetComponent<Image>().color = DefaultAvatarColor;
        AvatarsScrollView.GetChild(newSelectedIndex).GetComponent<Image>().color = ActiveAvatarColor;

        //changeAvatar
        CurrentAvatar.sprite = AvartarsList[newSelectedIndex].Image;
        IDIndexSkin = AvartarsList[newSelectedIndex].Id;
        
    }
}
