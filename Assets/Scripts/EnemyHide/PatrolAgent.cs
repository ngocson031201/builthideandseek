﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;
public class PatrolAgent : MonoBehaviour
{
    public static PatrolAgent instance;
    [SerializeField] private Transform[] points;
    [SerializeField] float RenmainingDistance ;

    public float waitTime = 4f;
    float time = 0;
    public Transform GroundCheck;
    [SerializeField] float GroundCheckDistance = 0.4f;
    public LayerMask groundSwimMask;

    public GameObject cage; //cái lồng

    bool isGrounded;
    Animator animator;
    bool isMove;

    int destinationPoint = 0;
    public NavMeshAgent agent;

    public bool isArrested;
    public static int HideArrested;

    private float currentSpeedAgent;

    //public GameObject HideSkinCharactor;
    public GameObject[] skins;

    [SerializeField] ParticleSystem EffectSmoke;
    bool isAIHide;
    public Transform AICheck;
    public LayerMask AIMask;
    public int selectedIndex;
    private void Awake()
    {
        HideArrested = 0;
        instance = this;

    }

    private void Start()
    {
        
        agent = GetComponent<NavMeshAgent>();
        agent.autoBraking = false;
        currentSpeedAgent = agent.speed;
        animator = GetComponentInChildren<Animator>();
        if (UIController.instance.isPlayGame == true)
        {
            moveToFirstPoint();
        }

        changeSkins();
    }

    private void changeSkins()
    {
        selectedIndex = Random.Range(0, skins.Length);
        skins[selectedIndex].SetActive(true);
        for (int i = 0; i < skins.Length; i++)
        {
            if (i != selectedIndex)
            {
                skins[i].SetActive(false);
            }
        }
    }

    public void DOScalePlayerSeek()
    {
        switch (HideArrested)
        {
            case 1:
                PlayerController.instance.transformParent.DOScale(new Vector3(1.1f, 1.1f, 1.1f), 5f).SetEase(Ease.OutBack);
                break;
            case 2:
                PlayerController.instance.transformParent.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 5f).SetEase(Ease.OutBack);
                break;
            case 3:
                PlayerController.instance.transformParent.DOScale(new Vector3(1.3f, 1.3f, 1.3f), 3f).SetEase(Ease.OutBack);
                break;
            case 4:
                PlayerController.instance.transformParent.DOScale(new Vector3(1.4f, 1.4f, 1.4f), 4f).SetEase(Ease.OutBack);
                break;
            case 5:
                PlayerController.instance.transformParent.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 5f).SetEase(Ease.OutBack);
                break;
        }
    }    

    private void Update()
    {

       // DOScalePlayerSeek();
        if (SpawnManager.ChangeStatic == true)//ps: cái này là chứng minh người chơi chọn hide not seek
        {
            skins[selectedIndex].SetActive(true);//er02
        }

        if (PlayerController.checkBulletXray == true)
        {
            // HideSkinCharactor.SetActive(true);//er02
            skins[selectedIndex].SetActive(true);//er02
        }

        if (UIController.instance.isPlayGame == false)
        {
            agent.speed = 0f;
            animator.SetFloat("MoveAnim", 0);
            animator.SetFloat("IdleAnim", 1);
            isMove = false;
        }

        if (cage.activeInHierarchy == true)
        {
            agent.speed = 0f;
            //  HideSkinCharactor.SetActive(true);//er03
            skins[selectedIndex].SetActive(true);//er02
            animator.SetFloat("MoveAnim", 0);
            animator.SetFloat("IdleAnim", 1);
        }

        if (!agent.pathPending && agent.remainingDistance < RenmainingDistance && UIController.instance.isPlayGame == true)
        {

            GoToNextPoint();
        }
        CheckGroundToSwim();
    }

    public void ShowCharactorAI(bool isoke)
    {
        // HideSkinCharactor.SetActive(isoke);//er02
        skins[selectedIndex].SetActive(isoke);//er02
    }

    public void Effect()
    {
        EffectSmoke.Play();
    }

    public void moveToFirstPoint()
    {
        //StartCoroutine(TimeBreak());

        int random = Random.Range(0, points.Length);
        agent.destination = points[random].position;
        animator.SetFloat("MoveAnim", 1);
    }
    public void GoToNextPoint()
    {
        StartCoroutine(TimeBreak());

        if (points.Length == 0)
        {
            Debug.Log(" bạn cần setup thêm point");
            enabled = false;
            return;
        }

        time += Time.deltaTime;
        if (time > waitTime)
        {
            Effect();
            agent.speed = currentSpeedAgent;
            animator.SetFloat("IdleAnim", 0);
            animator.SetFloat("MoveAnim", 1);
            agent.destination = points[destinationPoint].position;
            destinationPoint = (destinationPoint + 1) % points.Length;

            isMove = true;
            time = 0;

        }
        else
        {
            agent.speed = 0;
            animator.SetFloat("MoveAnim", 0);
            animator.SetFloat("IdleAnim", 1);
            isMove = false;

        }

    }

    public void CheckGroundToSwim()
    {
        isGrounded = Physics.CheckSphere(GroundCheck.position, GroundCheckDistance, groundSwimMask);
        if (isGrounded)
        {
            animator.SetBool("isGroundSwim", true);
        }
        else
        {
            animator.SetBool("isGroundSwim", false);
        }

        if (isGrounded && isMove == false)
        {
            animator.SetBool("isGroundSwimIdle", true);
        }

        if (isGrounded && isMove == true)
        {
            animator.SetBool("isGroundSwimIdle", false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        bool Checked = Physics.CheckSphere(GroundCheck.position, GroundCheckDistance, groundSwimMask);
        if (other.gameObject.name == "sensor")
        {

            if (cage.activeInHierarchy == true)
                return;

            // HideSkinCharactor.SetActive(true);//er03
            skins[selectedIndex].SetActive(true);//er02
            HideArrested++;

            isArrested = true;
            cage.SetActive(true);
            agent.speed = 0f;
            animator.SetFloat("MoveAnim", 0);

            if (HideArrested == 1 || HideArrested == 2 || HideArrested == 3)
            {
                ScoreManager.instance.AddCoinWithColisionHide(10);
            }

            if (HideArrested == 4 || HideArrested == 4)
            {
                ScoreManager.instance.AddCoinWithColisionHide(15);
            }

            if (Checked == true)
            {
                animator.SetBool("isGroundSwimIdle", true);
            }
            else
            {
                animator.SetFloat("IdleAnim", 1);
            }
        }

        if (other.gameObject.tag == "AIHide" && cage.activeInHierarchy == true)
        {
            HideArrested--;
            isArrested = false;
            cage.SetActive(false);
            //HideSkinCharactor.SetActive(false);//er05
            skins[selectedIndex].SetActive(false);//er02
            agent.speed = currentSpeedAgent;
            animator.SetFloat("MoveAnim", 1);
            animator.SetFloat("IdleAnim", 0);
        }

        if (other.gameObject.tag == "PlayerHide" && cage.activeInHierarchy == true)
        {
            HideArrested--;
            cage.SetActive(false);
            // HideSkinCharactor.SetActive(false);//er05
            skins[selectedIndex].SetActive(false);//er02
            ScoreManager.instance.AddCoinWithColisionHide(10);
            agent.speed = currentSpeedAgent;
            animator.SetFloat("MoveAnim", 1);
            animator.SetFloat("IdleAnim", 0);

        }

        if (other.gameObject.tag == "TrapSprites")
        {
            Destroy(this.gameObject);
        }

        if (other.gameObject.tag == "PaintsGlue")
        {
            StartCoroutine(changeMoveSpeedSlow());
        }

        if (other.gameObject.tag == "AIHide" && cage.activeInHierarchy == false)
        {
            transform.DORotate(new Vector3(0,transform.rotation.y+180f,0f),1f);
        }

    }

    public void CheckFloor()//check để xoay người 
    {
        isAIHide = Physics.CheckSphere(AICheck.position, 0.4f, AIMask);
        if (isAIHide && cage.activeInHierarchy == false)
        {
            agent.enabled = true;
        }

    }



    IEnumerator changeMoveSpeedSlow()
    {
        agent.speed = agent.speed / 2.5f;
        yield return new WaitForSeconds(5f);
        agent.speed = currentSpeedAgent;
    }

    IEnumerator TimeBreak()
    {
        yield return new WaitForSeconds(1.5f);
        if (SpawnManager.ChangeStatic == false && PlayerController.checkBulletXray == false)
        {
            //HideSkinCharactor.SetActive(false);//er06
            skins[selectedIndex].SetActive(false);//er02
        }

    }
}
