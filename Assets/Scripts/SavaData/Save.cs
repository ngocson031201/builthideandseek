using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

[System.Serializable]
public class Save
{
    public List<int> skinOwned;

    public Save( int idSave)
    {
        if (File.Exists(StaticVar.Path_SAVESHOP))
        {
            Save data = SaveSystem.LoadData<Save>(StaticVar.Path_SAVESHOP);
            skinOwned = data.skinOwned;
        }
        else
        {
            skinOwned = new List<int>();
            skinOwned.Add(0);
        }    

        if( !skinOwned.Contains(idSave))
        {
            skinOwned.Add(idSave);
        }
    }
}

